package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.UserDto;
import ru.t1.ytarasov.tm.enumirated.RoleType;
import ru.t1.ytarasov.tm.exception.AbstractException;

public interface IUserDtoService {

    @Transactional
    void create(@Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType) throws AbstractException;

    @Transactional
    void save(@Nullable final UserDto user) throws AbstractException;

    UserDto findByLogin(@Nullable final String login) throws AbstractException;

    boolean existsByLogin(@Nullable final String login) throws AbstractException;

}
