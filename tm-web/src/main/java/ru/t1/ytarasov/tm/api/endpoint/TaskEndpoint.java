package ru.t1.ytarasov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.dto.model.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface TaskEndpoint {

    @WebMethod
    @PutMapping("/create")
    TaskDto create() throws Exception;

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "task")
            @RequestBody TaskDto task
    ) throws Exception;

    @WebMethod
    @GetMapping("/findAll")
    Collection<TaskDto> findAll() throws Exception;

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDto findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) throws Exception;

    @WebMethod
    @GetMapping("/count")
    long count() throws Exception;

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    ) throws Exception;

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "id")
            @RequestBody TaskDto task
    ) throws Exception;

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "task")
            @PathVariable("id") String id
    ) throws Exception;

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "tasks")
            @RequestBody List<TaskDto> tasks
    ) throws Exception;

    @WebMethod
    @DeleteMapping("/clear")
    void clear() throws Exception;

}
