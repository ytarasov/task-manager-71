package ru.t1.ytarasov.tm.dto.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.util.DateUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "app_project")
public class ProjectDto {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(unique = true, nullable = false)
    private String name;

    @NotNull
    @Column(nullable = false)
    private String description;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date created = new Date();

    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date updated = new Date();

    @Column(name = "date_start")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateStart;

    @Column(name = "date_finish")
    @DateTimeFormat(pattern = DateUtil.PATTERN)
    private Date dateFinish;

    @Column(name = "user_id")
    private String userId;

    public ProjectDto() {
    }

    public ProjectDto(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
