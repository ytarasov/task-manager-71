package ru.t1.ytarasov.tm.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import ru.t1.ytarasov.tm.dto.model.Message;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ServiceAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(
            HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authenticationException
    ) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        @NotNull final PrintWriter writer = response.getWriter();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String value = authenticationException.getMessage();
        @NotNull final Message message = new Message(value);
        writer.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(message));
    }

}
