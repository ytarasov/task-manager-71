package ru.t1.ytarasov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.util.UserUtil;

import java.util.Collection;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectDtoService projectDtoService;

    public Collection<ProjectDto> getProjects() throws Exception {
        return projectDtoService.findByUserId(UserUtil.getUserId());
    }

    @GetMapping("/projects")
    public ModelAndView index() throws Exception {
        return new ModelAndView("projects", "projects", getProjects());
    }

}
