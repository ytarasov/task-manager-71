package ru.t1.ytarasov.tm.unit.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.configuration.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.marker.UnitWebCategory;
import ru.t1.ytarasov.tm.util.UserUtil;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@Category(UnitWebCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class ProjectControllerTest {

    @NotNull
    private final ProjectDto project1 = new ProjectDto("TEST PROJECT 1", "TEST PROJECT 1");

    @NotNull
    private final ProjectDto project2 = new ProjectDto("TEST PROJECT 2", "TEST PROJECT 2");

    @NotNull
    private final ProjectDto project3 = new ProjectDto("TEST PROJECT 2", "TEST PROJECT 3");

    @NotNull
    private final ProjectDto project4 = new ProjectDto("TEST PROJECT 4", "TEST PROJECT 4");

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @Nullable
    private String userId;

    private int count(@NotNull final String userId) throws Exception {
        return projectDtoService.countByUserId(userId).intValue();
    }

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        projectDtoService.saveWithUserId(userId, project1);
        projectDtoService.saveWithUserId(userId, project2);
    }

    @After
    public void tearDown() throws Exception {
        projectDtoService.deleteByUserId(userId);
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void createTest() throws Exception {
        @NotNull final String url = "/project/create";
        final int expectedSize = count(userId) + 1;
        mockMvc.perform(MockMvcRequestBuilders.post(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        final int foundSize = count(userId);
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull final String url = "/project/delete/" + project2.getId();
        final int expectedSize = count(userId) - 1;
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        final int foundSize = count(userId);
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void editTest() throws Exception {
        @NotNull final String url = "/project/edit/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
