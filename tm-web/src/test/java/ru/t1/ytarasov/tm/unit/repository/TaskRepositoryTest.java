package ru.t1.ytarasov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.ITaskDtoRepository;
import ru.t1.ytarasov.tm.configuration.*;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.marker.UnitWebCategory;

import java.util.List;
import java.util.UUID;

@Transactional
@WebAppConfiguration
@Category(UnitWebCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class TaskRepositoryTest {

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    @Autowired
    private ITaskDtoRepository taskDtoRepository;

    private TaskDto task;

    @Before
    public void setUp() {
        task = new TaskDto("TEST TASK", "TEST TASK");
        task.setUserId(USER_ID);
        taskDtoRepository.save(task);
    }

    @After
    public void tearDown() {
        taskDtoRepository.deleteByUserId(USER_ID);
    }

    @Test
    public void findByUserId() {
        final List<TaskDto> tasks = taskDtoRepository.findByUserId(USER_ID);
        Assert.assertNotNull(tasks);
        Assert.assertFalse(tasks.isEmpty());
    }

    public void findByUserIdAndId() {
        Assert.assertNotNull(taskDtoRepository.findByUserIdAndId(USER_ID, task.getId()));
    }

    public void existsByUserIdAndId() {
        Assert.assertTrue(taskDtoRepository.existsByUserIdAndId(USER_ID, task.getId()));
    }

    @Test
    public void countByUserId() {
        final int expectedSize = taskDtoRepository.findByUserId(USER_ID).size();
        final int foundSize = taskDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void delete() {
        @NotNull final TaskDto taskToDelete = new TaskDto("TEST DELETE", "TEST DELETE");
        taskToDelete.setUserId(USER_ID);
        taskDtoRepository.save(taskToDelete);
        final int expectedSize = taskDtoRepository.countByUserId(USER_ID).intValue() - 1;
        taskDtoRepository.deleteByUserIdAndId(USER_ID, taskToDelete.getId());
        final int foundSize = taskDtoRepository.countByUserId(USER_ID).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void deleteByUserId() {
        @NotNull final TaskDto taskToDelete = new TaskDto("TEST DELETE BY USERID", "TEST DELETE  BY USERID");
        @NotNull final String newUserId = "TST_USER_ID_2";
        taskDtoRepository.save(taskToDelete);
        taskToDelete.setUserId(newUserId);
        taskDtoRepository.deleteByUserId(newUserId);
        Assert.assertEquals(0, taskDtoRepository.countByUserId(newUserId).intValue());
    }

}
