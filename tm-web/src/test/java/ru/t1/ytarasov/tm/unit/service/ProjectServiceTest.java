package ru.t1.ytarasov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.ytarasov.tm.api.service.IProjectDtoService;
import ru.t1.ytarasov.tm.configuration.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.marker.UnitWebCategory;
import ru.t1.ytarasov.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@Category(UnitWebCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        SecurityWebApplicationInitializer.class,
        ServiceAuthenticationEntryPoint.class,
        WebApplicationConfiguration.class
})
public class ProjectServiceTest {

    @NotNull
    private final ProjectDto project1 = new ProjectDto("TEST PROJECT 1", "TEST PROJECT 1");

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private String userId;

    @Before
    public void setUp() throws Exception {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        projectDtoService.save(project1);
    }

    @After
    public void tearDown() throws Exception {
        projectDtoService.deleteByUserId(userId);
    }

    @Test
    public void findByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findByUserId(""));
        @Nullable final List<ProjectDto> projects = projectDtoService.findByUserId(userId);
        Assert.assertNotNull(projects);
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void findsByUserIdAndId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findByUserIdAndId(null, project1.getUserId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.findByUserIdAndId("", project1.getUserId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.findByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.findByUserIdAndId(userId, ""));
        @Nullable final ProjectDto project = projectDtoService.findByUserIdAndId(userId, project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.getName(), project.getName());
    }

    @Test
    public void countByUserId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.countByUserId(null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.countByUserId(""));
        final int expectedSize = projectDtoService.findByUserId(userId).size();
        final int foundSize = projectDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void existsByUserIdAndId() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.existsByUserIdAndId(null, project1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.existsByUserIdAndId(null, project1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.existsByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(projectDtoService.existsByUserIdAndId(userId, project1.getId()));
    }

    @Test
    public void deleteByUserIdAndId() throws Exception {
        @NotNull final ProjectDto projectToDelete = new ProjectDto("TEST DELETE", "TEST DELETE");
        projectToDelete.setUserId(userId);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.deleteByUserIdAndId(null, project1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectDtoService.deleteByUserIdAndId(null, project1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.deleteByUserIdAndId(userId, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDtoService.deleteByUserIdAndId(userId, ""));
        projectDtoService.save(projectToDelete);
        final int expectedSize = projectDtoService.countByUserId(userId).intValue() - 1;
        projectDtoService.deleteByUserIdAndId(userId, projectToDelete.getId());
        final int foundSize = projectDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void saveWithUserId() throws Exception {
        @NotNull final ProjectDto projectToSave = new ProjectDto("TEST SAVE", "TEST SAVE");
        final int expectedSize = projectDtoService.countByUserId(userId).intValue() + 1;
        projectDtoService.saveWithUserId(userId,projectToSave);
        final int foundSize = projectDtoService.countByUserId(userId).intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

}
