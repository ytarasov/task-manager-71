package ru.t1.ytarasov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.ytarasov.tm.listener.AbstractListener;
import ru.t1.ytarasov.tm.enumerated.Role;

import java.util.List;

@Getter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    public ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    protected List<AbstractListener> listeners;

    @Nullable
    @Override
    public Role @Nullable [] getRoles() {
        return null;
    }

}
