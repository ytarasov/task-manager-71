package ru.t1.ytarasov.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.api.model.IListener;
import ru.t1.ytarasov.tm.api.service.ITokenService;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.event.ConsoleEvent;

@Component
public abstract class AbstractListener implements IListener {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Override
    public abstract void handler(@NotNull ConsoleEvent event) throws Exception;

    @Override
    public abstract @NotNull String getName();

    @Nullable
    @Override
    public abstract String getArgument();

    @Override
    public abstract @NotNull String getDescription();

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable String token) {
        tokenService.setToken(token);
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ": " + description;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }

}
